# RabbitMQ Messaging POC

This project created to demonstrate messaging concept using following tech;

 - RabbitMQ
 - Linux VM
 - PHP
 - Go programming language
 
# How it works?

We will have one VM running Ubuntu, installed RabbitMQ and Go language
dependencies. A publisher inside VM will send some system information to
messaging broker (RabbitMQ) while PHP application running at host will consume
the message and output nicer system information on terminal.

# Results

![](https://i.imgur.com/dim3Nd6.png)

publisher send a message inside VM

![](https://i.imgur.com/OxCA1ZJ.png)

PHP application cosume and display output at host