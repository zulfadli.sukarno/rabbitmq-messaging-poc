<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('192.168.56.104', 5672, 'vmadmin', 'vmadmin');
$channel = $connection->channel();

$channel->queue_declare('system', false, false, false, false);

echo "Daemon started. Waiting for message. To exit press CTRL+C\n";

$callback = function ($msg) {
    $system_info = json_decode($msg->body);
    echo "---------- SYSTEM INFORMATION -------------\n";
    echo "System Uptime: " . $system_info->SystemUptimeMin . " Minutes\n";
    echo "Total RAM: " . $system_info->SystemTotalRamMB . " MB\n";
    echo "Free RAM: " . $system_info->SystemFreeRamMB . " MB\n";
    echo "Total Disk Space: " . substr($system_info->TotalDiskSpaceGB, 0, 4) . " GB\n";
    echo "Total Free Space: " . substr($system_info->TotalFreeSpaceGB, 0, 4) . " GB\n";
    echo "-------------------------------------------\n";
};

$channel->basic_consume('system', '', false, true, false, false, $callback);
while (count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();
?>
