#!/bin/bash

# create vmadmin user
rabbitmqctl add_user vmadmin vmadmin

# assign vmadmin user to administrator role
rabbitmqctl set_user_tags vmadmin administrator

# set permission
rabbitmqctl set_permissions -p / vmadmin ".*" ".*" ".*"