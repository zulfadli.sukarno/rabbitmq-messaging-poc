package main

import (
	"fmt"
	"log"
	"strconv"
	"syscall"

	"github.com/streadway/amqp"
)

type DiskStatus struct {
	All  uint64 `json:"all"`
	Used uint64 `json:"used"`
	Free uint64 `json:"free"`
}

type SystemStatus struct {
	Uptime   int64  `json:"uptime"`
	TotalRam uint64 `json:"totalram"`
	FreeRam  uint64 `json:"freeram"`
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

// disk usage of path/disk
func DiskUsage(path string) (disk DiskStatus) {
	fs := syscall.Statfs_t{}
	err := syscall.Statfs(path, &fs)
	if err != nil {
		return
	}
	disk.All = fs.Blocks * uint64(fs.Bsize)
	disk.Free = fs.Bfree * uint64(fs.Bsize)
	disk.Used = disk.All - disk.Free
	return
}

func SystemInfo() (system SystemStatus) {
	info := syscall.Sysinfo_t{}
	err := syscall.Sysinfo(&info)

	if err != nil {
		fmt.Println("Error:", err)
	}
	system.Uptime = info.Uptime
	system.TotalRam = info.Totalram
	system.FreeRam = info.Freeram
	return
}

const (
	B  = 1
	KB = 1024 * B
	MB = 1024 * KB
	GB = 1024 * MB
)

func main() {
	//get system information
	disk := DiskUsage("/")
	system := SystemInfo()
	// connect to message broker
	conn, err := amqp.Dial("amqp://vmadmin:vmadmin@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()
	// open and declare channel
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"system", // name
		false,    // durable
		false,    // delete when unused
		false,    // exclusive
		false,    // no-wait
		nil,      // arguments
	)
	failOnError(err, "Failed to declare a queue")

	sysUptime := strconv.FormatInt(system.Uptime/60, 10)
	TotRAM := strconv.FormatUint(system.TotalRam/uint64(MB), 10)
	FreRAM := strconv.FormatUint(system.FreeRam/uint64(MB), 10)
	totDisk := strconv.FormatFloat(float64(disk.All)/float64(GB), 'f', -1, 64)
	totFree := strconv.FormatFloat(float64(disk.Free)/float64(GB), 'f', -1, 64)

	// prepare json of system data
	sysinfo := "{ \"SystemUptimeMin\":\"%s\", \"SystemTotalRamMB\": \"%s\", \"SystemFreeRamMB\":\"%s\", \"TotalDiskSpaceGB\":\"%s\", \"TotalFreeSpaceGB\":\"%s\"  }"
	jsonSys := fmt.Sprintf(sysinfo, sysUptime, TotRAM, FreRAM, totDisk, totFree)

	// send to message broker
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(jsonSys),
		})
	log.Printf(" [x] Sent %s", jsonSys)
	failOnError(err, "Failed to publish a message")

	// fmt.Println(jsonSys)

	// fmt.Printf("All: %.2f GB\n", float64(disk.All)/float64(GB))
	// fmt.Printf("Used: %.2f GB\n", float64(disk.Used)/float64(GB))
	// fmt.Printf("Free: %.2f GB\n", float64(disk.Free)/float64(GB))
	// fmt.Printf("Uptime: %v min\n", system.Uptime/60)
	// fmt.Printf("TotalRam: %v GB\n", system.TotalRam/uint64(GB))
	// fmt.Printf("FreeRam: %v GB\n", system.FreeRam/uint64(GB))
}
